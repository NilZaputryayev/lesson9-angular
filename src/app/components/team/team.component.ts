import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})



export class TeamComponent implements OnInit {
  @Input() public team: Team;
  @Output() removedTeam : EventEmitter<Team> = new EventEmitter();
  @Output() hasChanges : EventEmitter<boolean> = new EventEmitter();

  public editMode : boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(private teamService: TeamService) 
  {}

  ngOnInit() {     
  } 
  
  deleteTeam()
  {
    if(confirm("Are you sure to delete team: " + this.team.name)) {
        this.teamService
            .deleteTeam(this.team.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        this.removedTeam.emit(this.team);                                                     
                },
                (error) => alert(error)
            );
        }  
          
}


  saveTeam(team :Team) {
    this.updateTeam()
  }

  updateTeam()
  {
    this.teamService
            .updateTeam(this.team)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        alert("Team updated success");
                        this.hasChanges.emit(false);                                                 
                },
                (error) => alert(error)
            );
  }


  editTeam() {
    this.editMode = !this.editMode;
    this.hasChanges.emit(this.editMode);
  }

 };
