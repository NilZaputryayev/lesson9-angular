import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})


export class TaskComponent implements OnInit {
  @Input() public task: Task;
  @Output() removedTask : EventEmitter<Task> = new EventEmitter();
  @Output() hasChanges : EventEmitter<boolean> = new EventEmitter();

  public editMode : boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(private taskService: TaskService) 
  {}

  ngOnInit() {     
  } 
  
  deleteTask()
  {
    if(confirm("Are you sure to delete task: " + this.task.name)) {
        this.taskService
            .deleteTask(this.task.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        this.removedTask.emit(this.task);                                                     
                },
                (error) => alert(error)
            );
        }  
          
}


  saveTask(task :Task) {
    this.updateTask()
  }

  updateTask()
  {
    this.taskService
            .updateTask(this.task)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        alert("Task updated success");
                        this.hasChanges.emit(false);                                                     
                },
                (error) => alert(error)
            );
  }


  editTask() {
    this.editMode = !this.editMode;
    this.hasChanges.emit(this.editMode);
  }
}
