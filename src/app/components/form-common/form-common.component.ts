import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-form-common',
  templateUrl: './form-common.component.html',
  styleUrls: ['./form-common.component.css']
})
export class FormCommonComponent<T> implements OnInit {
  @Input() item : T;
  @Output() createdItem : EventEmitter<T> = new EventEmitter(); 
  

  public editMode : boolean = false;
  public initState : T = {} as T;

  constructor() {
  }

  ngOnInit(): void {
  }


  saveItem() {
    console.log(this.item);
    this.createdItem.emit(this.item);
  }

  cancelEdit() {
    this.item = JSON.parse(JSON.stringify(this.initState));
  }

  clearForm() {
    this.item = {} as T;
  }
}
