import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user';
import { userService } from 'src/app/services/user.service';
import { Subject, Operator } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() public user: User;
  @Output() removedUser : EventEmitter<User> = new EventEmitter();
  @Output() hasChanges : EventEmitter<boolean> = new EventEmitter();


  public editMode : boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(private userService: userService) 
  {}

  ngOnInit() {     
  } 
  
  deleteUser()
  {
    if(confirm("Are you sure to delete user: " + this.user.firstName))
        {
        this.userService
            .deleteUser(this.user.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        this.removedUser.emit(this.user);                                                     
                },
                (error) => alert(error)
            );
        }  
}


  saveUser(project :User) {
    this.updateUser()
  }

  updateUser()
  {
    this.userService
            .updateUser(this.user)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        console.log("User update success");
                        this.hasChanges.emit(false);                                                
                },
                (error) => alert(error)
            );
  }


  editUser() {
    this.editMode = !this.editMode;
    this.hasChanges.emit(this.editMode);
  }
}
