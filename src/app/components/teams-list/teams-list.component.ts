import { Component, Input, OnInit } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import { Team } from 'src/app/models/team';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit {
  @Input() hasChanges : boolean = false;
  public addFormVisible : boolean = false;
  public teams : Team[] = [];
  public emptyTeam : Team = {} as Team;
  private unsubscribe$ = new Subject<void>();

  constructor( private teamService : TeamService) 
  {}

  ngOnInit() {
     this.getTeams();
  }

 public getTeams(){
   this.teamService
           .getTeams()
           .pipe(takeUntil(this.unsubscribe$))
           .subscribe(
               (resp) => {
                 if(resp.body)                 
                   this.teams =  resp.body;
               },
               (error) => (console.log(error.message))
           );
 }

 removeTeam(team : Team){
   this.teams.splice(this.teams.indexOf(team),1);
 }

 saveTeam(newTeam : Team) {
   this.teamService
           .createTeam(newTeam)
           .pipe(takeUntil(this.unsubscribe$))
           .subscribe(
               (resp) => {
                 if(resp.body)                 
                 this.teams.push(newTeam);
                 newTeam = {} as Team;
                 this.scrollToBottom();
               },
               (error) => (console.log(error.message))
           );   
   
 }

 scrollToBottom() {
   window.scrollTo(0,document.body.scrollHeight+100);
   this.addFormVisible = !this.addFormVisible;
 }


 showAddForm(){
   window.scroll({ 
     top: 0, 
     left: 0, 
     behavior: 'smooth' 
   });
   this.addFormVisible = !this.addFormVisible;
   this.emptyTeam = {} as Team;
 }

  setChangesState(hasChanges:boolean) {
    this.hasChanges = hasChanges;
    console.log(hasChanges);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.hasChanges) {
      return true;
    }  
    return confirm('Discard changes?');
  }

}
