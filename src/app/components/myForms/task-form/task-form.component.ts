import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { FormCommonComponent } from '../../form-common/form-common.component';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})

export class TaskFormComponent extends FormCommonComponent<Task> implements OnInit {
  ngOnInit(): void {
    this.initState = JSON.parse(JSON.stringify(this.item));    
    console.log(this.editMode);
    if(this.item.id !== undefined) this.editMode = true;  
  }

}
