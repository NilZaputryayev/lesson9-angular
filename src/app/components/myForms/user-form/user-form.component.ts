import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FormCommonComponent } from '../../form-common/form-common.component';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent extends FormCommonComponent<User> implements OnInit {
  ngOnInit(): void {
    this.initState = JSON.parse(JSON.stringify(this.item));    
    console.log(this.editMode);
    if(this.item.id !== undefined) this.editMode = true;  
  }

}
