import { Component, OnInit, Output, Input} from '@angular/core';
import { Project } from 'src/app/models/project';
import { FormCommonComponent } from '../../form-common/form-common.component';


@Component({
  selector: 'app-add-project-form',
  templateUrl: './add-project-form.component.html',
  styleUrls: ['./add-project-form.component.css']
})
export class AddProjectFormComponent extends FormCommonComponent<Project> implements OnInit {

  ngOnInit(): void {
    this.initState = JSON.parse(JSON.stringify(this.item));    
    console.log(this.editMode);
    if(this.item.id !== undefined) this.editMode = true;    
  }

}
