import { Component, OnInit } from '@angular/core';
import { FormCommonComponent } from '../../form-common/form-common.component';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.component.html',
  styleUrls: ['./team-form.component.css']
})
export class TeamFormComponent extends FormCommonComponent<Team> implements OnInit {
  ngOnInit(): void {
    this.initState = JSON.parse(JSON.stringify(this.item));    
    console.log(this.editMode);
    if(this.item.id !== undefined) this.editMode = true;  
  }

}






