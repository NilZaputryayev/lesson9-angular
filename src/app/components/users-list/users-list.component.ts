import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { userService } from 'src/app/services/user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  @Input() hasChanges : boolean = false;

  public addFormVisible : boolean = false;
  public users : User[] = [];
  public emptyUser : User = {} as User;
  private unsubscribe$ = new Subject<void>();

  constructor( private userService : userService) 
  {}

  ngOnInit() {
     this.getUsers();
  }

  public getUsers(){
    this.userService
            .getUsers()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                  if(resp.body)                 
                    this.users =  resp.body;
                },
                (error) => (console.log(error.message))
            );
  }

  removeUser(project : User){
    this.users.splice(this.users.indexOf(project),1);
  }

  saveUser(newUser : User) {
    this.userService
            .createUser(newUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                  if(resp.body)                 
                  this.users.push(newUser);
                  newUser = {} as User;
                  this.scrollToBottom();
                },
                (error) => (console.log(error.message))
            );   
    
  }

  scrollToBottom() {
    window.scrollTo(0,document.body.scrollHeight+100);
    this.addFormVisible = !this.addFormVisible;
  }


  showAddForm(){
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
    this.addFormVisible = !this.addFormVisible;
    this.emptyUser = {} as User;
  }

  setChangesState(hasChanges:boolean) {
    this.hasChanges = hasChanges;
    console.log(hasChanges);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.hasChanges) {
      return true;
    }  
    return confirm('Discard changes?');
  }

  
}
