import { Component, OnInit, Input } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { Task } from 'src/app/models/task';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable} from 'rxjs';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {
  @Input() hasChanges : boolean = false;

  public addFormVisible : boolean = false;
  public tasks : Task[] = [];
  public emptyTask : Task = {} as Task;
  private unsubscribe$ = new Subject<void>();

  constructor( private taskService : TaskService) 
  {}

  ngOnInit() {
     this.getTasks();
  }

 public getTasks(){
   this.taskService
           .getTasks()
           .pipe(takeUntil(this.unsubscribe$))
           .subscribe(
               (resp) => {
                 if(resp.body)                 
                   this.tasks =  resp.body;
               },
               (error) => (console.log(error.message))
           );
 }

 removeTask(task : Task){
   this.tasks.splice(this.tasks.indexOf(task),1);
 }

 saveTask(newTask : Task) {
   this.taskService
           .createTask(newTask)
           .pipe(takeUntil(this.unsubscribe$))
           .subscribe(
               (resp) => {
                 if(resp.body)                 
                 this.tasks.push(newTask);
                 newTask = {} as Task;
                 this.scrollToBottom();
               },
               (error) => (console.log(error.message))
           );   
   
 }

 scrollToBottom() {
   window.scrollTo(0,document.body.scrollHeight+100);
   this.addFormVisible = !this.addFormVisible;
 }


 showAddForm(){
   window.scroll({ 
     top: 0, 
     left: 0, 
     behavior: 'smooth' 
   });
   this.addFormVisible = !this.addFormVisible;
   this.emptyTask = {} as Task;
 }

 setChangesState(hasChanges:boolean) {
  this.hasChanges = hasChanges;
  console.log(hasChanges);
}

canDeactivate(): Observable<boolean> | boolean {
  if (!this.hasChanges) {
    return true;
  }  
  return confirm('Discard changes?');
}

}
