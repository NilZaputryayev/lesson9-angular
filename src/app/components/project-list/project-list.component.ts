import { Component, OnInit, Input } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { takeUntil } from 'rxjs/operators';
import { Subject,Observable } from 'rxjs';


@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {
  @Input() hasChanges : boolean = false;

  public addFormVisible : boolean = false;
  public projects : Project[] = [];
  public emptyProject : Project = {} as Project;
  private unsubscribe$ = new Subject<void>();
  
  constructor( private projectService : ProjectService) 
  {}

  ngOnInit() {
     this.getProjects();
  }

  public getProjects(){
    this.projectService
            .getProjects()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                  if(resp.body)                 
                    this.projects =  resp.body;
                },
                (error) => (console.log(error.message))
            );
  }

  removeProject(project : Project){
    this.projects.splice(this.projects.indexOf(project),1);
  }

  saveProject(newProject : Project) {
    this.projectService
            .createProject(newProject)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                  if(resp.body)                 
                  this.projects.push(newProject);
                  newProject = {} as Project;
                  this.scrollToBottom();
                },
                (error) => (console.log(error.message))
            );   
    
  }

  scrollToBottom() {
    window.scrollTo(0,document.body.scrollHeight+100);
    this.addFormVisible = !this.addFormVisible;
  }


  showAddForm(){
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
    this.addFormVisible = !this.addFormVisible;
//    const openForm= document.querySelector('add-container');
//    if(openForm) (openForm as HTMLElement).style.display = 'none';
    this.emptyProject = {} as Project;
  }

  setChangesState(hasChanges:boolean) {
    this.hasChanges = hasChanges;
    console.log(hasChanges);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.hasChanges) {
      return true;
    }  
    return confirm('Discard changes?');
  }

}
