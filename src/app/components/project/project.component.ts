import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Project } from 'src/app/models/project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
  @Input() public project: Project;
  @Output() removedProject : EventEmitter<Project> = new EventEmitter();
  @Output() hasChanges : EventEmitter<boolean> = new EventEmitter();

  public editMode : boolean = false;
  private unsubscribe$ = new Subject<void>();

  constructor(private projectService: ProjectService) 
  {}

  ngOnInit() {     
  } 
  
  deleteProject()
  {
    if(confirm("Are you sure to delete project: " + this.project.name))
        {
        this.projectService
            .deleteProject(this.project.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        this.removedProject.emit(this.project);
                                                 
                },
                (error) => alert(error)
            );
        }  
          
}


  saveProject(project :Project) {
    this.updateProject()
  }

  updateProject()
  {
    this.projectService
            .updateProject(this.project)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                        alert("Project saved.");
                        this.hasChanges.emit(false);                                               
                },
                (error) => alert(error)                
            );
  }


  editProject() {
  //    this.hideAllOpen();  
      this.editMode = !this.editMode;
      this.hasChanges.emit(this.editMode);
  }

  hideAllOpen() {
    const openForm= document.querySelector('.edit-form');
    if(openForm) (openForm as HTMLElement).style.display = 'none';
  }



}
