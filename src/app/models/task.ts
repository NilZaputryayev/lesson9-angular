export interface Task {
    id : number,
    performerId : number,
    projectId: number,
    name : string,
    description : string,
    state: number,
    createdAt : Date,
    finishedAt? : Date

}