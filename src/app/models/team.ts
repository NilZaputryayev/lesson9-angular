export interface Team {
    createdAt : string,
    id: number,
    name: string
}