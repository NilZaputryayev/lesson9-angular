import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectComponent } from './components/project/project.component';
import { TeamComponent } from './components/team/team.component';
import { TaskComponent } from './components/task/task.component';
import { UserComponent } from './components/user/user.component';
import { DateLessonPipeFromDate } from './pipes/date-date.pipe';
import { CapitalizeString } from './pipes/capitalize-pipe';
import { HttpClientModule } from '@angular/common/http';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { MainComponent } from './components/main/main.component';
import { TasksListComponent } from './components/tasks-list/tasks-list.component';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { FormsModule } from '@angular/forms';
import { AddProjectFormComponent } from './components/myForms/add-project-form/add-project-form.component';
import { FormCommonComponent } from './components/form-common/form-common.component';
import { TaskFormComponent } from './components/myForms/task-form/task-form.component';
import { TeamFormComponent } from './components/myForms/team-form/team-form.component';
import { UserFormComponent } from './components/myForms/user-form/user-form.component';
import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { TaskStateDirective } from './directives/task-state.directive';




@NgModule({
  declarations: [
    AppComponent,
    ProjectComponent,
    TeamComponent,
    TaskComponent,
    UserComponent,
    DateLessonPipeFromDate,
    CapitalizeString,
    ProjectListComponent,
    MainComponent,
    TasksListComponent,
    TeamsListComponent,
    UsersListComponent,
    FormCommonComponent,
    AddProjectFormComponent,
    TaskFormComponent,
    TeamFormComponent,
    UserFormComponent,
    HeaderMenuComponent,
    TaskStateDirective
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialComponentsModule 

  ],
  exports: [MaterialComponentsModule],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule {}
