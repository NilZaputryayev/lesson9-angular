import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamsListComponent } from './components/teams-list/teams-list.component';
import { TasksListComponent } from './components/tasks-list/tasks-list.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { CanDeactivateGuard } from './guard/can-deactivate.guard';

const routes: Routes = [
  { path: 'teams', component: TeamsListComponent, canDeactivate: [CanDeactivateGuard] },
  { path: 'projects', component: ProjectListComponent, canDeactivate: [CanDeactivateGuard]},
  { path: 'tasks', component: TasksListComponent, canDeactivate: [CanDeactivateGuard]},
  { path: 'users', component: UsersListComponent, canDeactivate: [CanDeactivateGuard]},
  { path: '',   redirectTo: '/projects', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
