import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective {
  @Input() state : number;
  public color : string = "blue";

  constructor(private el: ElementRef) {     
  }
  
  ngOnInit() {
    this.el.nativeElement.style.color = this.state === 1 ? "green" : 
                                        this.state === 2 ? "red" : "blue";    
  }

 
}
