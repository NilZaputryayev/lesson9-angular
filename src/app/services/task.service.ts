import { Injectable } from '@angular/core';
import { Task } from '../models/task';
import { HttpInternalService } from './http-internal.service'

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public routePrefix = '/task';

  constructor(private httpService: HttpInternalService) {}

  public getTasks() {
      return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
  }

  public createTask(task : Task) {
    return this.httpService.postFullRequest<Task>(`${this.routePrefix}`,task);
  }

  public updateTask(task : Task) {
    return this.httpService.putFullRequest<Task>(`${this.routePrefix}/${task.id}`,task);
  }

  public deleteTask(id:number) {
    return this.httpService.deleteRequest(`${this.routePrefix}/${id}`);
  }

}

