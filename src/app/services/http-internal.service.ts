import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HttpInternalService {
    public baseUrl: string = environment.apiUrl;

    constructor(private http: HttpClient) {}

  
    public getRequest<T>(url: string, httpParams?: any): Observable<T> {
        return this.http.get<T>(this.buildUrl(url), { params: httpParams });
    }

    public getFullRequest<T>(url: string, httpParams?: any): Observable<HttpResponse<T>> {
        return this.http.get<T>(this.buildUrl(url), { observe: 'response', params: httpParams });
    }

    public postClearRequest<T>(url: string, payload: object): Observable<T> {
        return this.http.post<T>(this.buildUrl(url), payload);
    }

    public postRequest<T>(url: string, payload: object): Observable<T> {
        return this.http.post<T>(this.buildUrl(url), payload );
    }

    public postFullRequest<T>(url: string, payload: object): Observable<HttpResponse<T>> {
        return this.http.post<T>(this.buildUrl(url), payload, { observe: 'response' });
    }

    public putRequest<T>(url: string, payload: object): Observable<T> {
        return this.http.put<T>(this.buildUrl(url), payload);
    }

    public putFullRequest<T>(url: string, payload: object): Observable<HttpResponse<T>> {
        return this.http.put<T>(this.buildUrl(url), payload, { observe: 'response'});
    }

    public deleteRequest<T>(url: string, httpParams?: any): Observable<T> {
        return this.http.delete<T>(this.buildUrl(url));
    }

    public deleteFullRequest<T>(url: string, httpParams?: any): Observable<HttpResponse<T>> {
        return this.http.delete<T>(this.buildUrl(url), { observe: 'response'});
    }

    public buildUrl(url: string): string {
        if (url.startsWith('http://') || url.startsWith('https://')) {
            return url;
        }
        return this.baseUrl + url;
    }

    public prepareData(payload: object): string {
        return JSON.stringify(payload);
    }
}
