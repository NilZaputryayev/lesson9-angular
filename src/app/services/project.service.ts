import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { HttpInternalService } from './http-internal.service'

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
public routePrefix = '/project';

constructor(private httpService: HttpInternalService) {}

public getProjects() {
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
}

public createProject(project : Project) {
  return this.httpService.postFullRequest<Project>(`${this.routePrefix}`,project);
}

public updateProject(project : Project) {
  return this.httpService.putFullRequest<Project>(`${this.routePrefix}/${project.id}`,project);
}

public deleteProject(id:number) {
  return this.httpService.deleteRequest(`${this.routePrefix}/${id}`);
}

}