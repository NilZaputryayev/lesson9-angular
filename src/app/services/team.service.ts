import { Injectable } from '@angular/core';
import { Team } from '../models/team';
import { HttpInternalService } from './http-internal.service'

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public routePrefix = '/team';

  constructor(private httpService: HttpInternalService) {}

  public getTeams() {
      return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
  }

  public createTeam(team : Team) {
    return this.httpService.postFullRequest<Team>(`${this.routePrefix}`,team);
  }

  public updateTeam(team : Team) {
    return this.httpService.putFullRequest<Team>(`${this.routePrefix}/${team.id}`,team);
  }

  public deleteTeam(id:number) {
    return this.httpService.deleteRequest(`${this.routePrefix}/${id}`);
  }

}

