import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpInternalService } from './http-internal.service'

@Injectable({
  providedIn: 'root'
})
export class userService {
  public routePrefix = '/user';

  constructor(private httpService: HttpInternalService) {}

  public getUsers() {
      return this.httpService.getFullRequest<User[]>(`${this.routePrefix}`);
  }

  public createUser(user : User) {
    return this.httpService.postFullRequest<User>(`${this.routePrefix}`,user);
  }

  public updateUser(user : User) {
    return this.httpService.putFullRequest<User>(`${this.routePrefix}/${user.id}`,user);
  }

  public deleteUser(id:number) {
    return this.httpService.deleteRequest(`${this.routePrefix}/${id}`);
  }

}

