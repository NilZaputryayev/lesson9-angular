import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'capitalize'})
export class CapitalizeString implements PipeTransform {
  transform(value: string): string {
    return value[0].toUpperCase() + value.substring(1,value.length-1);
  }
}