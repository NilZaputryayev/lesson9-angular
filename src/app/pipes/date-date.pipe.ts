import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'dateLessonTaskFromDate'})
export class DateLessonPipeFromDate implements PipeTransform {
  transform(value: Date | string): string {
    try {
      
        const date  = typeof(value) === typeof(Date) ? (value as Date) :  Date.parse(value as string)      
        const ye = new Intl.DateTimeFormat('uk-UA', { year: 'numeric' }).format(date);
        const mo = new Intl.DateTimeFormat('uk-UA', { month: 'long' }).format(date);
        const da = new Intl.DateTimeFormat('uk-UA', { day: '2-digit' }).format(date);
        const dt = `${da} ${mo} ${ye}`;    
        return dt;    
    }
    catch {
      return 'Wrong date';
    }
  }
}